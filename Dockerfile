FROM php:latest
MAINTAINER Aslam Idrisov <aslambek.idrisov@phbern>


RUN apt-get update \
	&& apt-get install -y \
			 git \
	  	   	 wget \
			 zip \
			 libzip-dev \
			 rsync \
			 openssh-client \
	&& docker-php-ext-install zip \
	&& rm -rf /var/lib/apt/lists/*


RUN eval $(ssh-agent -s)

RUN mkdir -p ~/.ssh \
	&& chmod 700 ~/.ssh

# install composer
RUN \
    curl -sS https://getcomposer.org/installer | php \
    && mv composer.phar /usr/local/bin/composer


SHELL [ "/bin/bash", "-l", "-c" ]

# Install Node
RUN curl --silent -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash \
	&& source ~/.nvm/nvm.sh \
	&& nvm install lts/* \
	&& nvm --version

# Install Deployer
RUN curl -LO https://deployer.org/deployer.phar \
	&& mv deployer.phar /usr/local/bin/dep \
	&& chmod +x /usr/local/bin/dep
